<?xml version="1.0" encoding="utf-8"?>
<!-- Copyright (C) 2009 The Android Open Source Project

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.
-->

<!-- These resources are around just to allow their values to be customized
     for different hardware and product builds.  Do not translate. -->
<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">
    <!-- Flag indicating which package name can access the persistent data partition -->
    <string name="config_persistentDataPackageName" translatable="false">com.google.android.gms</string>

    <!-- Set this to true to enable the platform's auto-power-save modes like doze and
         app standby.  These are not enabled by default because they require a standard
         cloud-to-device messaging service for apps to interact correctly with the modes
         (such as to be able to deliver an instant message to the device even when it is
         dozing).  This should be enabled if you have such services and expect apps to
         correctly use them when installed on your device.  Otherwise, keep this disabled
         so that applications can still use their own mechanisms. -->
    <bool name="config_enableAutoPowerModes">true</bool>

    <!-- Package name(s) containing location provider support. These packages will be auto-granted
         several permissions by the system, and should be system packages. -->
    <string-array name="config_locationProviderPackageNames" translatable="false">
        <item>com.google.android.gms</item>
        <!-- The standard AOSP fused location provider -->
        <item>com.android.location.fused</item>
    </string-array>

    <!-- A list of potential packages, in priority order, that may contain an
         ephemeral resolver. Each package will be be queried for a component
         that has been granted the PACKAGE_EPHEMERAL_AGENT permission.
         This may be empty if ephemeral apps are not supported. -->
    <string-array name="config_ephemeralResolverPackage" translatable="false">
        <item>com.google.android.gms</item>
    </string-array>

    <!-- The package name of the default network recommendation app.
         A network recommendation provider must:
             * Be granted the SCORE_NETWORKS permission.
             * Be granted the ACCESS_COARSE_LOCATION permission.
             * Include a Service for the android.net.scoring.RECOMMEND_NETWORKS action
               protected by the BIND_NETWORK_RECOMMENDATION_SERVICE permission.

         This must be set to a valid network recommendation app or empty.
     -->
    <string name="config_defaultNetworkRecommendationProviderPackage" translatable="false">com.google.android.gms</string>

    <!-- Component name that accepts ACTION_SEND intents for nearby (proximity-based) sharing.
         Used by ChooserActivity. -->
    <string translatable="false" name="config_defaultNearbySharingComponent">com.google.android.gms/com.google.android.gms.nearby.sharing.ShareSheetActivity</string>

    <!-- The component name, flattened to a string, for the default accessibility service to be
         enabled by the accessibility shortcut. This service must be trusted, as it can be activated
         without explicit consent of the user. If no accessibility service with the specified name
         exists on the device, the accessibility shortcut will be disabled by default. -->
    <string name="config_defaultAccessibilityService" translatable="false">com.google.android.marvin.talkback/.TalkBackService</string>

    <!-- The component name, flattened to a string, for the default autofill service
         to  enabled for an user. This service must be trusted, as it can be activated
         without explicit consent of the user. If no autofill service with the
          specified name exists on the device, autofill will be disabled by default.
    -->
    <string name="config_defaultAutofillService" translatable="false">com.google.android.gms/.autofill.service.AutofillService</string>

    <!-- Flag indicating which package name can access DeviceConfig table [DO NOT TRANSLATE] -->
    <string name="config_deviceConfiguratorPackageName" translatable="false">com.google.android.gms</string>

    <!-- A list of potential packages, in priority order, that can supply rules to
         AppIntegrityManager. These need to be apps on the system partition. -->
    <string-array name="config_integrityRuleProviderPackages" translatable="false">
        <item>com.android.vending</item>
        <item>com.google.android.gms</item>
    </string-array>

    <!-- The set of system packages on device that are queryable regardless of the contents of their
         manifest. -->
    <string-array name="config_forceQueryablePackages" translatable="false">
        <item>com.android.settings</item>
        <item>com.google.android.gms</item>
        <item>com.android.vending</item>
    </string-array>

    <!-- An array of packages for which notifications cannot be blocked.
         Should only be used for core device functionality that must not be
         rendered inoperative for safety reasons, like the phone dialer and
         SMS handler. -->
    <string-array name="config_nonBlockableNotificationPackages" translatable="false">
        <item>com.google.android.deskclock:Firing</item>
        <item>com.google.android.setupwizard</item>
        <item>com.google.android.apps.pixelmigrate</item>
        <item>com.google.android.dialer</item>
    </string-array>

    <!-- An array of packages that can make sound on the ringer stream in priority-only DND
         mode -->
    <string-array name="config_priorityOnlyDndExemptPackages" translatable="false">
        <item>com.google.android.dialer</item>
        <item>com.android.server.telecom</item>
        <item>android</item>
        <item>com.android.systemui</item>
    </string-array>

    <!-- Names of packages that should not be suspended when personal use is blocked by policy. -->
    <string-array name="config_packagesExemptFromSuspension" translatable="false">
        <item>com.google.android.apps.wellbeing</item>
        <item>com.android.vending</item>
        <item>com.google.android.gms</item>
        <item>com.android.systemui</item>
        <item>com.android.settings</item>
        <item>com.google.android.apps.pixelmigrate</item>
        <item>com.google.android.apps.restore</item>
    </string-array>

    <!-- The package name for the system's app prediction service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         Example: "com.android.intelligence/.AppPredictionService"
    -->
    <string name="config_defaultAppPredictionService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiPredictionService</string>

    <!-- The package name for the system's augmented autofill service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         If no service with the specified name exists on the device, augmented autofill wil be
         disabled.
         Example: "com.android.augmentedautofill/.AugmentedAutofillService"
    -->
    <string name="config_defaultAugmentedAutofillService">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiAugmentedAutofillService</string>

    <!-- The package name for the system's content capture service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         If no service with the specified name exists on the device, content capture will be
         disabled.
         Example: "com.android.contentcapture/.ContentcaptureService"
    -->
    <string name="config_defaultContentCaptureService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiContentCaptureService</string>

    <!-- The package name for the system's content suggestions service.
         Provides suggestions for text and image selection regions in snapshots of apps and should
         be able to classify the type of entities in those selections.
         This service must be trusted, as it can be activated without explicit consent of the user.
         If no service with the specified name exists on the device, content suggestions wil be
         disabled.
         Example: "com.android.contentsuggestions/.ContentSuggestionsService"
    -->
    <string name="config_defaultContentSuggestionsService">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiContentSuggestionsService</string>

    <!-- Colon separated list of package names that should be granted DND access -->
    <string name="config_defaultDndAccessPackages" translatable="false">com.google.android.gms:com.google.android.apps.wellbeing:com.google.android.dialer</string>

    <!-- Colon separated list of package names that should be granted Notification Listener access -->
    <string name="config_defaultListenerAccessPackages" translatable="false">com.google.android.apps.nexuslauncher:com.google.android.setupwizard:com.google.android.apps.pixelmigrate:com.google.android.as</string>

    <!-- The component name for the default profile supervisor, which can be set as a profile owner
         even after user setup is complete. The defined component should be used for supervision purposes
         only. The component must be part of a system app. -->
    <string name="config_defaultSupervisionProfileOwnerComponent" translatable="false">com.google.android.gms/.kids.account.receiver.ProfileOwnerReceiver</string>

    <!-- The component name for the system-wide captions manager service.
         This service must be trusted, as the system binds to it and keeps it running.
         Example: "com.android.captions/.SystemCaptionsManagerService"
    -->
    <string name="config_defaultSystemCaptionsManagerService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.captions.SystemCaptionsManagerService</string>

    <!-- The component name for the system-wide captions service.
         This service must be trusted, as it controls part of the UI of the volume bar.
         Example: "com.android.captions/.SystemCaptionsService"
    -->
    <string name="config_defaultSystemCaptionsService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.captions.CaptionsService</string>

    <!-- The package name for the default system textclassifier service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         Example: "com.android.textclassifier"
         If no textclassifier service with the specified name exists on the device (or if this is
         set to empty string), a default textclassifier will be loaded in the calling app's process.
         See android.view.textclassifier.TextClassificationManager.
    -->
    <string name="config_defaultTextClassifierPackage">com.google.android.as</string>

    <!-- The package name for the default wellbeing app.
         This package must be trusted, as it has the permissions to control other applications
         on the device.
         Example: "com.android.wellbeing"
     -->
    <string name="config_defaultWellbeingPackage" translatable="false">com.google.android.apps.wellbeing</string>

    <!-- The name of the package that will hold the assistant role by default. -->
    <string name="config_defaultAssistant" translatable="false">com.google.android.googlequicksearchbox</string>

    <!-- Component name that should be granted Notification Assistant access -->
    <string name="config_defaultAssistantAccessComponent" translatable="false">com.google.android.ext.services/android.ext.services.notification.Assistant</string>

    <!-- The name of the package that will hold the dialer role by default. -->
    <string name="config_defaultDialer" translatable="false">com.google.android.dialer</string>

    <!-- The name of the package that will hold the SMS role by default. -->
    <string name="config_defaultSms" translatable="false">com.google.android.apps.messaging</string>

    <!-- The name of the package that will hold the system gallery role. -->
    <string name="config_systemGallery" translatable="false">com.google.android.apps.photos</string>

    <!-- Package name for default keyguard appwidget [DO NOT TRANSLATE] -->
    <string name="widget_default_package_name" translatable="false">com.google.android.deskclock</string>

    <!-- Class name for default keyguard appwidget [DO NOT TRANSLATE] -->
    <string name="widget_default_class_name" translatable="false">com.android.alarmclock.DigitalAppWidgetProvider</string>

    <!-- The list of IMEs which should be disabled until used.
         This function suppresses update notifications for these pre-installed apps.
         We need to set this configuration carefully that they should not have functionarities
         other than "IME" or "Spell Checker". In InputMethodManagerService,
         the listed IMEs are disabled until used when all of the following conditions are met.
         1. Not selected as an enabled IME in the Settings
         2. Not selected as a spell checker in the Settings
         3. Installed
         4. A pre-installed IME
         5. Not enabled
         And the disabled_until_used state for an IME is released by InputMethodManagerService
         when the IME is selected as an enabled IME. -->
    <string-array name="config_disabledUntilUsedPreinstalledImes" translatable="false">
        <item>com.google.android.inputmethod.latin</item>
        <item>com.google.android.apps.inputmethod.hindi</item>
        <item>com.google.android.inputmethod.japanese</item>
        <item>com.google.android.inputmethod.korean</item>
        <item>com.google.android.inputmethod.pinyin</item>
    </string-array>

    <!-- An array of packages that need to be treated as type service in battery settings -->
    <string-array translatable="false" name="config_batteryPackageTypeService">
        <item>com.google.android.gms</item>
    </string-array>

    <!-- Component name for the activity that will be presenting the Recents UI, which will receive
         special permissions for API related to fetching and presenting recent tasks. The default
         configuration uses Launcehr3QuickStep as default launcher and points to the corresponding
         recents component. When using a different default launcher, change this appropriately or
         use the default systemui implementation: com.android.systemui/.recents.RecentsActivity -->
    <string name="config_recentsComponentName" translatable="false"
            >com.google.android.apps.nexuslauncher/com.android.quickstep.RecentsActivity</string>

    <!-- This is the default launcher package with an activity to use on secondary displays that
         support system decorations.
         This launcher package must have an activity that supports multiple instances and has
         corresponding launch mode set in AndroidManifest.
         {@see android.view.Display#FLAG_SHOULD_SHOW_SYSTEM_DECORATIONS} -->
    <string name="config_secondaryHomePackage" translatable="false">com.google.android.apps.nexuslauncher</string>

    <!-- The app which will handle routine based automatic battery saver, if empty the UI for
         routine based battery saver will be hidden -->
    <string name="config_batterySaverScheduleProvider">com.google.android.apps.turbo</string>

    <!-- Flag indicating whether we should enable smart battery. -->
	<bool name="config_smart_battery_available">true</bool>

    <!-- The list of components which should be automatically disabled for all devices. -->
    <string-array name="config_globallyDisabledComponents" translatable="false">
        <item>com.android.vending/com.google.android.finsky.systemupdate.SystemUpdateSettingsContentProvider</item>
        <item>com.android.vending/com.google.android.finsky.systemupdateactivity.SettingsSecurityEntryPoint</item>
        <item>com.android.vending/com.google.android.finsky.systemupdateactivity.SystemUpdateActivity</item>
        <item>com.google.android.as/com.google.intelligence.sense.ambientmusic.history.HistoryContentProvider</item>
        <item>com.google.android.as/com.google.intelligence.sense.ambientmusic.history.HistoryActivity</item>
        <item>com.google.android.as/com.google.intelligence.sense.ambientmusic.AmbientMusicSettingsActivity</item>
        <item>com.google.android.as/com.google.intelligence.sense.ambientmusic.AmbientMusicNotificationsSettingsActivity</item>
        <item>com.google.android.as/com.google.intelligence.sense.ambientmusic.AmbientMusicSetupWizardActivity</item>
        <item>com.google.android.gms/com.google.android.gms.update.SystemUpdateActivity</item>
        <item>com.google.android.gms/com.google.android.gms.update.SystemUpdateGcmTaskService</item>
        <item>com.google.android.gms/com.google.android.gms.update.SystemUpdateService</item>
    </string-array>
</resources>
